import { Module } from '@nestjs/common';
import { RethinkProvider } from './rethink.provider';

@Module({
  controllers: [],
  providers: [RethinkProvider],
  exports: [RethinkProvider],
})
export class RethinkModule {}
