import * as rethink from 'rethinkdb';

export const RethinkProvider = {
  provide: 'RethinkProvider',
  useFactory: async () => {
    return await rethink.connect({
      host: '192.168.1.208',
      port: 28015,
      db: 'mifuka',
    });
  },
};
