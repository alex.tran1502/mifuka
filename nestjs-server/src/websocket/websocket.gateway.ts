import { Controller, UseGuards, ValidationPipe } from '@nestjs/common';
import { WebsocketService } from './websocket.service';
import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ApiBearerAuth } from '@nestjs/swagger';
import { WebsocketAuthGuard } from '../auth/websocket.strategy';
import { GetWebsocketUser } from '../auth/getuser.decorator';

@WebSocketGateway()
@ApiBearerAuth()
@UseGuards(WebsocketAuthGuard)
export class WebsocketGateway {
  @WebSocketServer()
  server: Server;

  constructor(private readonly websocketService: WebsocketService) {}

  @SubscribeMessage('joinRoom')
  create(
    @MessageBody() msg: unknown,
    @ConnectedSocket() socket: Socket,
    @GetWebsocketUser() user,
  ) {
    console.log('New client Room Created: ', user);
    socket.join(user);
    return `You joined room ${user}`;
  }
}
