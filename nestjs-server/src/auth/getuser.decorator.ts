import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

export const GetUser = createParamDecorator((data, ctx: ExecutionContext) => {
  const req = ctx.switchToHttp().getRequest();
  return req.user;
});

export const GetWebsocketUser = createParamDecorator(
  (data, ctx: ExecutionContext) => {
    const req = ctx.switchToWs().getClient();
    const bearerToken = req.handshake.headers.authorization.split(' ')[1];
    const decoded = jwt.decode(bearerToken);
    return decoded['username'];
  },
);
