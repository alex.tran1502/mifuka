import {
  Controller,
  Request,
  Post,
  UseGuards,
  Get,
  Body,
  BadRequestException,
} from '@nestjs/common';
import { LocalAuthGuard } from './local.strategy';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt.strategy';
import { GetUser } from './getuser.decorator';
import { ApiTags } from '@nestjs/swagger';

@Controller('auth')
@ApiTags('Authentication')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Get('/user')
  @UseGuards(JwtAuthGuard)
  getUser(@GetUser() user) {
    return { user: user };
  }

  @Post('validateToken')
  async validate(@Body() body) {
    return this.authService.validateToken(body.token);
  }
}
