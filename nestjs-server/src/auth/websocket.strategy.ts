import { CanActivate, Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class WebsocketAuthGuard implements CanActivate {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async canActivate(context: any) {
    const bearerToken = context.args[0].handshake.headers.authorization.split(
      ' ',
    )[1];

    try {
      const { username } = await this.jwtService.verifyAsync(bearerToken);
      const user = await this.usersService.findOne(username);

      return !!user;
    } catch (e) {
      throw new UnauthorizedException('Token Expired');
    }
  }
}
