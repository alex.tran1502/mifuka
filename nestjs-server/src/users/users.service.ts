import { Inject, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import * as r from 'rethinkdb';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  private readonly connection: r.Connection;
  constructor(@Inject('RethinkProvider') connection) {
    this.connection = connection;
  }

  async create(createUserDto: CreateUserDto) {
    const saltRound = 5;
    const password = await bcrypt.hash(createUserDto.password, saltRound);

    return await r
      .table('users')
      .insert({
        username: createUserDto.username,
        password,
      })
      .run(this.connection);
  }

  async findAll() {
    const docs = await r.table('users').run(this.connection);
    return await docs.toArray();
  }

  async findOne(username: string): Promise<any> {
    return await r.table('users').get(username).run(this.connection);
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
