import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { RethinkModule } from './rethink/rethink.module';
import { BoardsModule } from './boards/boards.module';
import { WebsocketModule } from './websocket/websocket.module';
import { AuthModule } from './auth/auth.module';
import { ProjectsModule } from './projects/projects.module';

@Module({
  imports: [
    UsersModule,
    RethinkModule,
    BoardsModule,
    WebsocketModule,
    AuthModule,
    ProjectsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
