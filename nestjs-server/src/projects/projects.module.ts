import { Module } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ProjectsController } from './projects.controller';
import { RethinkModule } from 'src/rethink/rethink.module';
import { WebsocketModule } from '../websocket/websocket.module';

@Module({
  imports: [RethinkModule, WebsocketModule],
  controllers: [ProjectsController],
  providers: [ProjectsService],
})
export class ProjectsModule {}
