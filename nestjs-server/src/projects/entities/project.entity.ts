export class Project {
  id: number;
  name: string;
  description: string;
  owner: string;
  created_at: Date;
  boards: Array<any>;
}
