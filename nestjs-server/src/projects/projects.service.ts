import { Inject, Injectable } from '@nestjs/common';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import * as r from 'rethinkdb';
import { Project } from './entities/project.entity';

@Injectable()
export class ProjectsService {
  private readonly connection: r.Connection;

  constructor(@Inject('RethinkProvider') connection) {
    this.connection = connection;
  }

  async create(createProjectDto: CreateProjectDto, username: string) {
    return await r
      .table('projects')
      .insert({
        name: createProjectDto.name,
        description: createProjectDto.description,
        owner: username,
        created_at: new Date(),
      })
      .run(this.connection);
  }

  async findAllOfUser(username: string): Promise<Project[]> {
    const docs = await r
      .table('projects')
      .filter({ owner: username })
      .run(this.connection);

    return await docs.toArray();
  }

  async findOne(id: string): Promise<Project> {
    return (await r.table('projects').get(id).run(this.connection)) as Project;
  }

  async update(id: number, updateProjectDto: UpdateProjectDto) {
    return await r
      .table('projects')
      .update({
        name: updateProjectDto.name,
        description: updateProjectDto.description,
      })
      .run(this.connection);
  }

  async remove(id: string) {
    return await r.table('projects').get(id).delete().run(this.connection);
  }
}
