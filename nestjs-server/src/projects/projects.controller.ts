import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt.strategy';
import { GetUser } from 'src/auth/getuser.decorator';
import { WebsocketGateway } from '../websocket/websocket.gateway';

@ApiTags('Projects')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('projects')
export class ProjectsController {
  constructor(
    private readonly projectsService: ProjectsService,
    private readonly websocketGateway: WebsocketGateway,
  ) {}

  @Post()
  create(
    @Body(ValidationPipe) createProjectDto: CreateProjectDto,
    @GetUser() user,
  ) {
    return this.projectsService.create(createProjectDto, user.username);
  }

  @Get()
  async findAllOfUser(@GetUser() user) {
    this.websocketGateway.server
      .to(user.username)
      .emit('testChannel', 'FindAllOfUser is triggered');
    return this.projectsService.findAllOfUser(user.username);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.projectsService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateProjectDto: UpdateProjectDto) {
    return this.projectsService.update(+id, updateProjectDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.projectsService.remove(id);
  }
}
