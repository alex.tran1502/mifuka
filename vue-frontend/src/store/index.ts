import { createStore } from 'vuex';

export default createStore({
  state: {
    socket: 'ideal',
  },
  mutations: {
    alo(state, payload) {
      state.socket = payload;
    },
  },
  actions: {
    openWebsocket(context) {
      console.log('openwebsocket');
      context.commit('alo', 'Connected');
    },
  },
  modules: {},
});
