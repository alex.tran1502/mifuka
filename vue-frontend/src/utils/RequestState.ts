export enum ApiRequestState {
  IDLE,
  LOADING,
  SUCCESS,
  FAILURE,
}
