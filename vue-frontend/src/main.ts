import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios, { AxiosRequestConfig } from 'axios';

// Setup axois interceptor to assign jwt token to header
axios.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    const token = localStorage.jwtToken;
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

createApp(App).use(store).use(router).mount('#app');
