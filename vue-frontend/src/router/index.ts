import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import Home from '../views/Home.vue';
import Login from '@/views/Login.vue';
import useLogin from '@/composable/useLogin';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach(async (to, from, next) => {
  try {
    const { validateToken } = useLogin();
    const isAuthenticated = await validateToken();
    if (to.name !== 'Login' && !isAuthenticated) next({ name: 'Login' });
    else next();
  } catch (e) {
    console.log('Navigation Error: ', e);
  }
});

export default router;
