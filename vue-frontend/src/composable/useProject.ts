import axios from 'axios';
import HTMLStatusCode from '@/utils/HTMLStatusCode';
import { ApiRequestState } from '@/utils/RequestState';
import { ref } from 'vue';

export default function useProject() {
  const getBoardState = ref<ApiRequestState>(ApiRequestState.IDLE);

  async function getBoards() {
    getBoardState.value = ApiRequestState.LOADING;
    try {
      const response = await axios.get('/projects');
      if (response.status == HTMLStatusCode.OK) {
        getBoardState.value = ApiRequestState.SUCCESS;
        console.log(response.data);
        return response.data;
      }
    } catch (e) {
      getBoardState.value = ApiRequestState.FAILURE;
      console.log(e);
    }
  }

  return {
    getBoardState,
    getBoards,
  };
}
