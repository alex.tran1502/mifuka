import axios from 'axios';

export default function useLogin() {
  async function authenticate(username: string, password: string): Promise<boolean> {
    try {
      const response = await axios.post('/auth/login', {
        username,
        password,
      });

      if (response.status == 201) {
        const { access_token } = response.data;
        localStorage.setItem('jwtToken', access_token);

        return true;
      }
    } catch (e) {
      alert(e);
    }

    return false;
  }

  async function validateToken() {
    try {
      const token = localStorage.jwtToken;

      const response = await axios.post('/auth/validateToken', {
        token,
      });

      if (response.status == 201) {
        return true;
      }
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  return {
    authenticate,
    validateToken,
  };
}
