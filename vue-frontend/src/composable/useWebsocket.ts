import { ref } from 'vue';
import { io, Socket } from 'socket.io-client';

export enum WebSocketState {
  IDLE = 'IDLE',
  CONNECTED = 'CONNECTED',
  DISCONNECTED = 'DISCONNECTED',
}

const socket = ref<Socket>();
const socketState = ref<WebSocketState>(WebSocketState.IDLE);

export default function useWebsocket() {
  function openConnection() {
    const token = localStorage.jwtToken;

    const socketOption = {
      transportOptions: {
        polling: {
          extraHeaders: {
            Authorization: `Bearer ${token}`,
          },
        },
      },
    };

    socket.value = io('/', socketOption);
  }

  function monitorWsState() {
    socket.value?.on('success', () => {
      console.log('Websocket Authenticated');
    });

    socket.value?.on('disconnect', () => {
      socketState.value = WebSocketState.DISCONNECTED;
    });

    socket.value?.on('exception', (e: unknown) => {
      console.log('SOCKET EXCEPTION: ', e);
    });

    socket.value?.on('error', (e: unknown) => {
      console.log('SOCKET ERROR: ', e);
    });

    socket.value?.on('roomCreated', (msg: unknown) => {
      console.log('Room Created: ', msg);
    });

    socket.value?.on('testChannel', (msg: unknown) => {
      console.log('[testChannel] Message from Server: ', msg);
    });

    socket.value?.on('connect', () => {
      socketState.value = WebSocketState.CONNECTED;
      socket.value?.emit('joinRoom', null, (res: string) => console.log(res));
    });
  }

  return {
    openConnection,
    monitorWsState,

    socketState,
    socket,
  };
}
